﻿#include <iostream>

void var (int n, int a)
{
    for (int i = a; i <= n; i += 2)
        std::cout << i << " ";
}

int main()
{
    int n=10;

    var(n, 0);
    std::cout << "\n";
    var(n, 1);
}